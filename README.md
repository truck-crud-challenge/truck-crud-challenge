# Truck CRUD Challenge

o Explain why you designed the API the way you did and what assumptions did you take.

I designed simple API, I did not use any authentication (tried to keep it simple).

Trucks can have:
- name
- horsepower
- displacement
- colors (from dictionary)
- application segments (from dictionary)
- range (from dictionary)
- fueltype (from dictionary)

 
o What kind of layers did you use?

I've used simple architecture with three layers: 
- web
- domain (business)
- db (persistence)

o Which approach has been used to store the data?
I've used h2 DB and MyBatis 

Explain the Angular components and how you did the integration with the OpenAPI
files.
- I generated rest client from OpenApi file.
- Tried to keep it simple
Components:
 - truck-list
 - truck-edit-dialog
 - confirm-dialog
 - dictionares-service


o Explain if you did use a version control system like Git to save intermediate states.
- Yes I did.
- There are two separate repos for server and client side application
