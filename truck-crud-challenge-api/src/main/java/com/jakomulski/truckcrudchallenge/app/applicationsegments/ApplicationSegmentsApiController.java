package com.jakomulski.truckcrudchallenge.app.applicationsegments;

import com.jakomulski.truckcrudchallenge.V1ApiController;
import com.jakomulski.truckcrudchallenge.rest.v1.api.ApplicationsegmentsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;

@V1ApiController
public class ApplicationSegmentsApiController implements ApplicationsegmentsApi {
    private ApplicationSegmentService applicationSegmentService;

    @Autowired
    public ApplicationSegmentsApiController(ApplicationSegmentService applicationSegmentService) {
        this.applicationSegmentService = applicationSegmentService;
    }

    @Override
    public ResponseEntity<List<String>> getApplicationSegments() {
        return ResponseEntity.ok(applicationSegmentService.getApplicationSegments());
    }
}
