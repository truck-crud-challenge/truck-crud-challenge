package com.jakomulski.truckcrudchallenge.app.color;

import com.jakomulski.truckcrudchallenge.V1ApiController;
import com.jakomulski.truckcrudchallenge.rest.v1.api.ColorsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;

@V1ApiController
public class ColorsApiController implements ColorsApi {
    private ColorService colorService;

    @Autowired
    public ColorsApiController(ColorService colorService) {
        this.colorService = colorService;
    }

    @Override
    public ResponseEntity<List<String>> getColors() {
        return ResponseEntity.ok(colorService.getColors());
    }
}
