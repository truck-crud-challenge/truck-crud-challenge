package com.jakomulski.truckcrudchallenge.app.fueltype;

import com.jakomulski.truckcrudchallenge.V1ApiController;
import com.jakomulski.truckcrudchallenge.rest.v1.api.FueltypesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;

@V1ApiController
public class FuelTypesApiController implements FueltypesApi {

    public FuelTypeService fuelTypeService;

    @Autowired
    public FuelTypesApiController(FuelTypeService fuelTypeService) {
        this.fuelTypeService = fuelTypeService;
    }

    @Override
    public ResponseEntity<List<String>> getFuelTypes() {
        return ResponseEntity.ok(fuelTypeService.getFuelTypes());
    }
}
