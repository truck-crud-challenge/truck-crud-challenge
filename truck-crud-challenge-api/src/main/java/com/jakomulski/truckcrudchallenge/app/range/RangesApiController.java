package com.jakomulski.truckcrudchallenge.app.range;

import com.jakomulski.truckcrudchallenge.V1ApiController;
import com.jakomulski.truckcrudchallenge.rest.v1.api.RangesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;

@V1ApiController
public class RangesApiController implements RangesApi {

    private RangeService rangesService;

    @Autowired
    public RangesApiController(RangeService rangesService) {
        this.rangesService = rangesService;
    }

    @Override
    public ResponseEntity<List<String>> getRanges() {
        return ResponseEntity.ok(rangesService.getRanges());
    }
}
