package com.jakomulski.truckcrudchallenge.app.truck;

import com.jakomulski.truckcrudchallenge.V1ApiController;
import com.jakomulski.truckcrudchallenge.exceptions.NameAlreadyExistsException;
import com.jakomulski.truckcrudchallenge.exceptions.NotFoundException;
import com.jakomulski.truckcrudchallenge.rest.v1.api.TrucksApi;
import com.jakomulski.truckcrudchallenge.rest.v1.model.Truck;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.List;

@V1ApiController
public class TrucksApiController implements TrucksApi {

    Logger logger = LoggerFactory.getLogger(TrucksApiController.class);

    private TruckService truckService;
    private ModelMapper modelMapper;

    @Autowired
    public TrucksApiController(TruckService truckService, ModelMapper modelMapper) {
        this.truckService = truckService;
        this.modelMapper = modelMapper;
    }

    @ResponseStatus(value = HttpStatus.CONFLICT,
            reason = "Name already exists")
    @ExceptionHandler(NameAlreadyExistsException.class)
    public void nameAlreadyExists(Exception e) {
        logger.warn("Name already exists", e);
    }

    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE,
            reason = "Invalid object")
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public void invalidArgument(MethodArgumentNotValidException e) {
        logger.warn("Invalid object", e);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND,
            reason = "Truck not found")
    @ExceptionHandler(NotFoundException.class)
    public void truckNotFound(Exception e) {
        logger.warn("Truck not found", e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Unknown error")
    @ExceptionHandler(Exception.class)
    public void unknownError(Exception e) {
        logger.error("Unknown error", e);
    }

    @Override
    public ResponseEntity<Truck> createTruck(@Valid Truck body) {
        TruckDto truckDto = modelMapper.map(body, TruckDto.class);
        truckDto = truckService.createTruck(truckDto);
        Truck result = modelMapper.map(truckDto, Truck.class);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteTruck(Long truckId) {
        truckService.deleteTruck(truckId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Truck> getTruck(Long truckId) {
        return truckService.getTruckById(truckId)
                .map(dto -> ResponseEntity.ok(modelMapper.map(dto, Truck.class)))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<List<Truck>> getTrucks() {
        List<Truck> trucks = modelMapper.map(
                truckService.getTrucks(),
                new TypeToken<List<Truck>>() {
                }.getType());
        return ResponseEntity.ok(trucks);
    }

    @Override
    public ResponseEntity<Truck> updateTruck(Long truckId, @Valid Truck body) {
        TruckDto truckDto = modelMapper.map(body, TruckDto.class);
        truckDto = truckService.updateTruck(truckId, truckDto);
        Truck result = modelMapper.map(truckDto, Truck.class);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
