package com.jakomulski.truckcrudchallenge.app.applicationsegments;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
class ApplicationSegmentsApiIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldGetAllApplicationSegments() {
        String url = getBaseUrl() + "/v1/applicationsegments";
        ResponseEntity<String[]> response = restTemplate.getForEntity(url, String[].class);
        List<String> actual = Arrays.asList(response.getBody());
        actual.sort(String::compareTo);

        Assert.assertEquals(List.of("Construction", "Distribution (Food)", "Firedepartment", "Long haul", "Wastedisposal"), actual);
    }

    private String getBaseUrl() {
        return "http://localhost:" + port;
    }
}