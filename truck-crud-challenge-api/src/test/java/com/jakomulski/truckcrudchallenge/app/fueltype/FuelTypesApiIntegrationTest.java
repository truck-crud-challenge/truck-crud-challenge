package com.jakomulski.truckcrudchallenge.app.fueltype;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
class FuelTypesApiIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldGetAllFuelTypes() {
        String url = getBaseUrl() + "/v1/fueltypes";
        ResponseEntity<String[]> response = restTemplate.getForEntity(url, String[].class);
        List<String> actual = Arrays.asList(response.getBody());
        actual.sort(String::compareTo);

        Assert.assertEquals(List.of("Diesel", "Natural gas"), actual);
    }

    private String getBaseUrl() {
        return "http://localhost:" + port;
    }
}