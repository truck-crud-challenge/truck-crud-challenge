package com.jakomulski.truckcrudchallenge.app.truck;

import com.jakomulski.truckcrudchallenge.rest.v1.model.Truck;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.util.Arrays;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
class TrucksApiIntegrationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void shouldGetTruck() {
        String url = getBaseUrl() + "/v1/trucks/1";
        ResponseEntity<Truck> response = restTemplate.getForEntity(url, Truck.class);
        Truck expected = new Truck()
                .id(1L)
                .name("truck_1")
                .horsepower(100)
                .displacement(200)
                .fueltype("Diesel")
                .range("heavy")
                .colors(Arrays.asList("black", "red", "white"))
                .applicationSegments(Arrays.asList("Construction", "Long haul"));

        Truck actual = response.getBody();
        actual.getColors().sort(String::compareTo);
        actual.getApplicationSegments().sort(String::compareTo);
        Assert.assertEquals(expected, actual);
    }

    @Test
    void shouldGetAllTrucks() {
        String url = getBaseUrl() + "/v1/trucks";
        ResponseEntity<Truck[]> response = restTemplate.getForEntity(url, Truck[].class);
        Truck[] actual = response.getBody();
        Assert.assertEquals("truck_1", actual[0].getName());
        Assert.assertEquals(2, actual.length);
    }


    @Test
    void shouldUpdateTruck() {
        String url = getBaseUrl() + "/v1/trucks/2";
        Truck requestBody = restTemplate.getForEntity(url, Truck.class).getBody()
                .name("truck_36543")
                .range("light")
                .displacement(100)
                .horsepower(100)
                .fueltype("Diesel")
                .colors(Arrays.asList("red", "green"))
                .applicationSegments(Arrays.asList("Firedepartment"));
        requestBody.getColors().sort(String::compareTo);
        requestBody.getApplicationSegments().sort(String::compareTo);

        // Assert that updateResponse body is the same as requestBody
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Truck> entity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<Truck> updateResponse = restTemplate.exchange(url, HttpMethod.PUT, entity, Truck.class);
        updateResponse.getBody().getColors().sort(String::compareTo);
        updateResponse.getBody().getApplicationSegments().sort(String::compareTo);

        Assert.assertEquals(HttpStatus.OK, updateResponse.getStatusCode());
        Assert.assertEquals(requestBody, updateResponse.getBody());

        // Assert that updateResponse body is the same as getResponse body
        ResponseEntity<Truck> getResponse = restTemplate.getForEntity(url, Truck.class);
        getResponse.getBody().getColors().sort(String::compareTo);
        getResponse.getBody().getApplicationSegments().sort(String::compareTo);
        Assert.assertEquals(getResponse.getBody(), updateResponse.getBody());
    }

    @Test
    void shouldCreateAndDeleteTruck() {
        String url = getBaseUrl() + "/v1/trucks";
        Truck requestBody = new Truck()
                .name("truck_04935")
                .range("light")
                .displacement(100)
                .horsepower(100)
                .fueltype("Diesel")
                .addColorsItem("red")
                .addColorsItem("blue")
                .addApplicationSegmentsItem("Long haul")
                .addApplicationSegmentsItem("Construction");
        requestBody.getColors().sort(String::compareTo);
        requestBody.getApplicationSegments().sort(String::compareTo);

        int initialNumberOfTrucks = restTemplate.getForEntity(url, Truck[].class).getBody().length;

        // Assert that createResponse body is the same as requestBody with set id
        ResponseEntity<Truck> createResponse = restTemplate.postForEntity(url, requestBody, Truck.class);
        createResponse.getBody().getColors().sort(String::compareTo);
        createResponse.getBody().getApplicationSegments().sort(String::compareTo);

        Assert.assertNotNull(createResponse.getBody().getId());
        Assert.assertEquals(requestBody.getName(), createResponse.getBody().getName());
        Assert.assertEquals(requestBody.getRange(), createResponse.getBody().getRange());
        Assert.assertEquals(requestBody.getDisplacement(), createResponse.getBody().getDisplacement());
        Assert.assertEquals(requestBody.getHorsepower(), createResponse.getBody().getHorsepower());
        Assert.assertEquals(requestBody.getFueltype(), createResponse.getBody().getFueltype());
        Assert.assertEquals(requestBody.getColors(), createResponse.getBody().getColors());
        Assert.assertEquals(requestBody.getApplicationSegments(), createResponse.getBody().getApplicationSegments());

        // Assert that createResponse body is the same as getResponse body
        String getUrl = url + "/" + createResponse.getBody().getId();
        ResponseEntity<Truck> getResponse = restTemplate.getForEntity(getUrl, Truck.class);
        getResponse.getBody().getColors().sort(String::compareTo);
        getResponse.getBody().getApplicationSegments().sort(String::compareTo);

        Assert.assertEquals(getResponse.getBody(), createResponse.getBody());

        //Assert that number of trucks is increased by 1
        Assert.assertEquals(initialNumberOfTrucks + 1, restTemplate.getForEntity(url, Truck[].class).getBody().length);

        //Assert that record is deleted correctly
        restTemplate.delete(url + "/" + createResponse.getBody().getId());
        Assert.assertEquals(initialNumberOfTrucks, restTemplate.getForEntity(url, Truck[].class).getBody().length);
    }

    @Test
    void shouldNotCreateTruckWithInvalidBody() {
        String url = getBaseUrl() + "/v1/trucks";
        Truck requestBody = new Truck()
                .name("truck_04935")
                .displacement(100)
                .horsepower(100)
                .fueltype("Diesel")
                .addColorsItem("red")
                .addColorsItem("blue")
                .addApplicationSegmentsItem("Long haul")
                .addApplicationSegmentsItem("Construction");
        requestBody.getColors().sort(String::compareTo);
        requestBody.getApplicationSegments().sort(String::compareTo);

        ResponseEntity<Truck> createResponse = restTemplate.postForEntity(url, requestBody, Truck.class);
        Assert.assertEquals(createResponse.getStatusCode(), HttpStatus.NOT_ACCEPTABLE);
    }

    @Test
    void shouldNotCreateTruckWithNotUniqueName() {
        String url = getBaseUrl() + "/v1/trucks";
        Truck requestBody = new Truck()
                .name("truck_1")
                .displacement(100)
                .horsepower(100)
                .range("light")
                .fueltype("Diesel")
                .addColorsItem("red")
                .addColorsItem("blue")
                .addApplicationSegmentsItem("Long haul")
                .addApplicationSegmentsItem("Construction");
        requestBody.getColors().sort(String::compareTo);
        requestBody.getApplicationSegments().sort(String::compareTo);

        ResponseEntity<Truck> createResponse = restTemplate.postForEntity(url, requestBody, Truck.class);
        Assert.assertEquals(createResponse.getStatusCode(), HttpStatus.CONFLICT);
    }

    @Test
    void shouldNotUpdateTruckToNotUniqueName() {
        String url = getBaseUrl() + "/v1/trucks/2";
        Truck requestBody = restTemplate.getForEntity(url, Truck.class).getBody()
                .name("truck_1");

        // Assert that there is conflict
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Truck> entity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<Truck> updateResponse = restTemplate.exchange(url, HttpMethod.PUT, entity, Truck.class);
        Assert.assertEquals(HttpStatus.CONFLICT, updateResponse.getStatusCode());
    }

    @Test
    void shouldNotUpdateTruckToInvalidBody() {
        String url = getBaseUrl() + "/v1/trucks/2";
        Truck requestBody = restTemplate.getForEntity(url, Truck.class).getBody()
                .fueltype(null);

        // Assert that there is 406
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Truck> entity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<Truck> updateResponse = restTemplate.exchange(url, HttpMethod.PUT, entity, Truck.class);
        Assert.assertEquals(HttpStatus.NOT_ACCEPTABLE, updateResponse.getStatusCode());
    }

    @Test
    void shouldGet404WhenGetNonExistingTruck() {
        String url = getBaseUrl() + "/v1/trucks/46";
        ResponseEntity<Truck> response = restTemplate.getForEntity(url, Truck.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void shouldGet404WhenUpdateNonExistingTruck() {
        String url = getBaseUrl() + "/v1/trucks/45";
        Truck requestBody = new Truck()
                .name("truck1")
                .range("light")
                .displacement(100)
                .horsepower(100)
                .fueltype("Diesel")
                .addColorsItem("red")
                .addColorsItem("blue")
                .addApplicationSegmentsItem("Long haul")
                .addApplicationSegmentsItem("Construction");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Truck> entity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<Truck> response = restTemplate.exchange(url, HttpMethod.PUT, entity, Truck.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void shouldGet404WhenDeleteNonExistingTruck() {
        String url = getBaseUrl() + "/v1/trucks/45";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Truck> entity = new HttpEntity<>(headers);
        ResponseEntity<Truck> response = restTemplate.exchange(url, HttpMethod.DELETE, entity, Truck.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    private String getBaseUrl() {
        return "http://localhost:" + port;
    }
}