INSERT INTO color(name)
VALUES ('black'),
       ('white'),
       ('red'),
       ('green'),
       ('yellow'),
       ('blue'),
       ('pink'),
       ('gray'),
       ('brown'),
       ('orange'),
       ('purple');

INSERT INTO application_segment(name)
VALUES ('Long haul'),
       ('Construction'),
       ('Firedepartment'),
       ('Distribution (Food)'),
       ('Wastedisposal');

INSERT INTO range(name)
VALUES ('heavy'),
       ('light');

INSERT INTO fuel_type(name)
VALUES ('Diesel'),
       ('Natural gas');

INSERT INTO truck(name, horsepower, displacement, fuel_type_id, range_id)
VALUES ('truck_1', 100, 200, 1, 1);

INSERT INTO colors_trucks(color_id, truck_id)
VALUES (1, 1),
       (2, 1),
       (3, 1);

INSERT INTO application_segments_trucks(application_segment_id, truck_id)
VALUES (1, 1),
       (2, 1);

INSERT INTO truck(name, horsepower, displacement, fuel_type_id, range_id)
VALUES ('truck_2', 200, 100, 2, 2);

INSERT INTO colors_trucks(color_id, truck_id)
VALUES (1, 2),
       (3, 2);

INSERT INTO application_segments_trucks(application_segment_id, truck_id)
VALUES (1, 2);