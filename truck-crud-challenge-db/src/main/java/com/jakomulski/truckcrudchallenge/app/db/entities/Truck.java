package com.jakomulski.truckcrudchallenge.app.db.entities;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

public class Truck {
    private long id;
    private String name;
    private String range;
    private int horsepower;
    private int displacement;
    private String fuelType;
    private List<String> colors;
    private List<String> applicationSegments;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getDisplacement() {
        return displacement;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public List<String> getApplicationSegments() {
        return applicationSegments;
    }

    public void setApplicationSegments(List<String> applicationSegments) {
        this.applicationSegments = applicationSegments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Truck truck = (Truck) o;

        return new EqualsBuilder()
                .append(id, truck.id)
                .append(horsepower, truck.horsepower)
                .append(displacement, truck.displacement)
                .append(name, truck.name)
                .append(range, truck.range)
                .append(fuelType, truck.fuelType)
                .append(colors, truck.colors)
                .append(applicationSegments, truck.applicationSegments)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(range)
                .append(horsepower)
                .append(displacement)
                .append(fuelType)
                .append(colors)
                .append(applicationSegments)
                .toHashCode();
    }
}
