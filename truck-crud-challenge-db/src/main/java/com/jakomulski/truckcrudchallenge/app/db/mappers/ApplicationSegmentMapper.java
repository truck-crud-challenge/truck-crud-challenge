package com.jakomulski.truckcrudchallenge.app.db.mappers;

import com.jakomulski.truckcrudchallenge.app.db.entities.ApplicationSegment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ApplicationSegmentMapper {
    @Select("SELECT * from application_segment")
    List<ApplicationSegment> getAll();
}
