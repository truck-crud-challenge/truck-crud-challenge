package com.jakomulski.truckcrudchallenge.app.db.mappers;

import com.jakomulski.truckcrudchallenge.app.db.entities.Color;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ColorMapper {
    @Select("SELECT * from color")
    List<Color> getAll();
}
