package com.jakomulski.truckcrudchallenge.app.db.mappers;

import com.jakomulski.truckcrudchallenge.app.db.entities.FuelType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FuelTypeMapper {
    @Select("SELECT * from fuel_type")
    List<FuelType> getAll();
}
