package com.jakomulski.truckcrudchallenge.app.db.mappers;

import com.jakomulski.truckcrudchallenge.app.db.entities.Range;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RangeMapper {
    @Select("SELECT * from range")
    List<Range> getAll();
}
