package com.jakomulski.truckcrudchallenge.app.db.mappers;

import com.jakomulski.truckcrudchallenge.app.db.entities.Truck;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TruckMapper {
    @Select({"select * from truck"})
    @Results({
            @Result(property = "id", javaType = Long.class, column = "id"),
            @Result(property = "range", javaType = String.class, column = "range_id", one = @One(select = "getRangeNameById")),
            @Result(property = "fuelType", javaType = String.class, column = "fuel_type_id", one = @One(select = "getFuelTypeNameById")),
            @Result(property = "colors", javaType = List.class, column = "id", many = @Many(select = "getColorNamesByTruckId")),
            @Result(property = "applicationSegments", javaType = List.class, column = "id", many = @Many(select = "getApplicationSegmentNamesByTruckId"))
    })
    List<Truck> getAll();

    @Select("SELECT * FROM truck WHERE id = #{id}")
    @Results({
            @Result(property = "id", javaType = Long.class, column = "id"),
            @Result(property = "range", javaType = String.class, column = "range_id", one = @One(select = "getRangeNameById")),
            @Result(property = "fuelType", javaType = String.class, column = "fuel_type_id", one = @One(select = "getFuelTypeNameById")),
            @Result(property = "colors", javaType = List.class, column = "id", many = @Many(select = "getColorNamesByTruckId")),
            @Result(property = "applicationSegments", javaType = List.class, column = "id", many = @Many(select = "getApplicationSegmentNamesByTruckId"))
    })
    Truck getTruckById(long id);

    @Select("SELECT name FROM range WHERE id = #{id}")
    List<String> getRangeNameById(long id);

    @Select("SELECT name FROM fuel_type WHERE id = #{id}")
    List<String> getFuelTypeNameById();

    @Select("SELECT c.name FROM color c JOIN colors_trucks ct ON c.id = ct.color_id WHERE ct.truck_id = #{truck_id};")
    List<String> getColorNamesByTruckId(long truckId);

    @Select("SELECT a.name FROM application_segment a JOIN application_segments_trucks ast ON a.id = ast.application_segment_id WHERE ast.truck_id = #{truck_id};")
    List<String> getApplicationSegmentNamesByTruckId(long truckId);

    @Insert("INSERT INTO truck(name, horsepower, displacement, fuel_type_id, range_id) " +
            "VALUES (#{name}, #{horsepower}, #{displacement}, (SELECT id FROM fuel_type WHERE name = #{fuelType}), (SELECT id FROM range WHERE name = #{range}))")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void createTruck(Truck truck);

    @Insert({"<script>",
            "insert into colors_trucks (color_id, truck_id)",
            "SELECT id, #{truckId} FROM color WHERE name IN ",
            "(<foreach collection='colors' item='name' separator = ', ' >#{name}</foreach>)",
            "</script>"})
    void addColors(long truckId, List<String> colors);

    @Delete({"<script>",
            "DELETE FROM colors_trucks WHERE truck_id = #{truckId} AND color_id NOT IN ",
            "(SELECT id FROM color WHERE id NOT IN (SELECT color_id FROM colors_trucks WHERE truck_id = #{truckId}) AND name IN ",
            "(<foreach collection='colors' item='name' separator = ', ' >#{name}</foreach>))",
            "</script>"})
    void deleteAllColorsBut(long truckId, List<String> colors);

    @Insert({"<script>",
            "insert into colors_trucks (color_id, truck_id)",
            "SELECT id, #{truckId} FROM color WHERE id NOT IN (SELECT color_id FROM colors_trucks WHERE truck_id = #{truckId}) AND name IN ",
            "(<foreach collection='colors' item='name' separator = ', ' >#{name}</foreach>)",
            "</script>"})
    void insertNonexistentColors(long truckId, List<String> colors);

    @Delete({"<script>",
            "DELETE FROM application_segments_trucks WHERE truck_id = #{truckId} AND application_segment_id NOT IN ",
            "(SELECT id FROM application_segment WHERE id NOT IN (SELECT application_segment_id FROM application_segments_trucks WHERE truck_id = #{truckId}) AND name IN ",
            "(<foreach collection='applicationSegments' item='name' separator = ', ' >#{name}</foreach>))",
            "</script>"})
    void deleteAllApplicationSegmentsBut(long truckId, List<String> applicationSegments);

    @Insert({"<script>",
            "insert into application_segments_trucks (application_segment_id, truck_id)",
            "SELECT id, #{truckId} FROM application_segment WHERE id NOT IN (SELECT application_segment_id FROM application_segments_trucks WHERE truck_id = #{truckId}) AND name IN ",
            "(<foreach collection='applicationSegments' item='name' separator = ', ' >#{name}</foreach>)",
            "</script>"})
    void insertNonexistentApplicationSegments(long truckId, List<String> applicationSegments);


    @Insert({"<script>",
            "insert into application_segments_trucks (application_segment_id, truck_id) values ",
            "<foreach collection='applicationSegments' item='name' separator = ', ' >((SELECT id FROM application_segment WHERE name = #{name}), #{truckId})</foreach>",
            "</script>"})
    void addApplicationSegments(long truckId, List<String> applicationSegments);

    @Delete("DELETE FROM truck WHERE id = #{id}")
    int deleteTruck(long id);

    @Update({"UPDATE truck SET name=#{truck.name}, horsepower=#{truck.horsepower}, displacement=#{truck.displacement}, ",
            "fuel_type_id=(SELECT id FROM fuel_type WHERE name = #{truck.fuelType}), ",
            "range_id=(SELECT id FROM range WHERE name = #{truck.range})",
            "WHERE id=#{truckId}"})
    int updateTruck(long truckId, Truck truck);
}
