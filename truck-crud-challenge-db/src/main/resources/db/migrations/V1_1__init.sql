CREATE TABLE color(
    id bigint auto_increment PRIMARY KEY,
	name varchar(100) UNIQUE NOT NULL
);

CREATE TABLE fuel_type(
    id bigint auto_increment PRIMARY KEY,
	name varchar(100) UNIQUE NOT NULL
);

CREATE TABLE application_segment(
    id bigint auto_increment PRIMARY KEY,
	name varchar(100) UNIQUE NOT NULL
);

CREATE TABLE range(
    id bigint auto_increment PRIMARY KEY,
	name varchar(100) UNIQUE NOT NULL
);

CREATE TABLE truck(
    id bigint auto_increment PRIMARY KEY,
	name varchar(100) UNIQUE NOT NULL,
	horsepower int NOT NULL,
	displacement int NOT NULL,
	range_id bigint NOT NULL,
	fuel_type_id bigint NOT NULL,
	foreign key (range_id) references range(id),
	foreign key (fuel_type_id) references fuel_type(id)
);

CREATE TABLE colors_trucks(
    color_id bigint NOT NULL,
    truck_id bigint NOT NULL,
    primary key(color_id, truck_id),
    foreign key (color_id) references color(id),
    foreign key (truck_id) references truck(id) ON DELETE CASCADE
);

CREATE TABLE application_segments_trucks(
    application_segment_id bigint NOT NULL,
    truck_id bigint NOT NULL,
    primary key(application_segment_id, truck_id),
    foreign key (application_segment_id) references application_segment(id),
    foreign key (truck_id) references truck(id) ON DELETE CASCADE
);