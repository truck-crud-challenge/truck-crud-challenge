INSERT INTO color(name)
VALUES ('black'),
       ('white'),
       ('red'),
       ('green'),
       ('yellow'),
       ('blue'),
       ('pink'),
       ('gray'),
       ('brown'),
       ('orange'),
       ('purple');

INSERT INTO application_segment(name)
VALUES ('Long haul'),
       ('Construction'),
       ('Firedepartment'),
       ('Distribution (Food)'),
       ('Wastedisposal');

INSERT INTO range(name)
VALUES ('heavy'),
       ('light');

INSERT INTO fuel_type(name)
VALUES ('Diesel'),
       ('Natural gas');