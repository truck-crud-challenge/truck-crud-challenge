package com.jakomulski.truckcrudchallenge.app.applicationsegments;

import com.jakomulski.truckcrudchallenge.app.db.entities.ApplicationSegment;
import com.jakomulski.truckcrudchallenge.app.db.mappers.ApplicationSegmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ApplicationSegmentService {

    private ApplicationSegmentMapper applicationSegmentMapper;

    @Autowired
    public ApplicationSegmentService(ApplicationSegmentMapper applicationSegmentMapper) {
        this.applicationSegmentMapper = applicationSegmentMapper;
    }

    public List<String> getApplicationSegments() {
        return applicationSegmentMapper.getAll().stream().map(ApplicationSegment::getName).collect(Collectors.toList());
    }
}
