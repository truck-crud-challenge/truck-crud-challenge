package com.jakomulski.truckcrudchallenge.app.color;

import com.jakomulski.truckcrudchallenge.app.db.entities.Color;
import com.jakomulski.truckcrudchallenge.app.db.mappers.ColorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
class ColorService {

    private ColorMapper colorMapper;

    @Autowired
    ColorService(ColorMapper colorMapper) {
        this.colorMapper = colorMapper;
    }

    List<String> getColors() {
        return colorMapper.getAll().stream().map(Color::getName).collect(Collectors.toList());
    }
}
