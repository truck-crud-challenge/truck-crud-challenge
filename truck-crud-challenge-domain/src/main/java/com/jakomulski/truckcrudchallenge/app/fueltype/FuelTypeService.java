package com.jakomulski.truckcrudchallenge.app.fueltype;

import com.jakomulski.truckcrudchallenge.app.db.entities.FuelType;
import com.jakomulski.truckcrudchallenge.app.db.mappers.FuelTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FuelTypeService {

    private FuelTypeMapper fuelTypeMapper;

    @Autowired
    public FuelTypeService(FuelTypeMapper fuelTypeMapper) {
        this.fuelTypeMapper = fuelTypeMapper;
    }

    public List<String> getFuelTypes() {
        return fuelTypeMapper.getAll().stream().map(FuelType::getName).collect(Collectors.toList());
    }
}
