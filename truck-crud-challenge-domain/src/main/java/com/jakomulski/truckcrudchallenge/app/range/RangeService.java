package com.jakomulski.truckcrudchallenge.app.range;

import com.jakomulski.truckcrudchallenge.app.db.entities.Range;
import com.jakomulski.truckcrudchallenge.app.db.mappers.RangeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RangeService {

    private RangeMapper rangeMapper;

    @Autowired
    public RangeService(RangeMapper rangeMapper) {
        this.rangeMapper = rangeMapper;
    }

    public List<String> getRanges() {
        return rangeMapper.getAll().stream().map(Range::getName).collect(Collectors.toList());
    }
}
