package com.jakomulski.truckcrudchallenge.app.truck;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

public class TruckDto {
    private long id;
    private String name;
    private String range;
    private int horsepower;
    private int displacement;
    private String fuelType;
    private List<String> colors;
    private List<String> applicationSegments;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public int getDisplacement() {
        return displacement;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public List<String> getApplicationSegments() {
        return applicationSegments;
    }

    public void setApplicationSegments(List<String> applicationSegments) {
        this.applicationSegments = applicationSegments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TruckDto truckDto = (TruckDto) o;

        return new EqualsBuilder()
                .append(id, truckDto.id)
                .append(horsepower, truckDto.horsepower)
                .append(displacement, truckDto.displacement)
                .append(name, truckDto.name)
                .append(range, truckDto.range)
                .append(fuelType, truckDto.fuelType)
                .append(colors, truckDto.colors)
                .append(applicationSegments, truckDto.applicationSegments)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(range)
                .append(horsepower)
                .append(displacement)
                .append(fuelType)
                .append(colors)
                .append(applicationSegments)
                .toHashCode();
    }
}
