package com.jakomulski.truckcrudchallenge.app.truck;


import com.jakomulski.truckcrudchallenge.app.db.entities.Truck;
import com.jakomulski.truckcrudchallenge.app.db.mappers.TruckMapper;
import com.jakomulski.truckcrudchallenge.exceptions.NameAlreadyExistsException;
import com.jakomulski.truckcrudchallenge.exceptions.NotFoundException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Component
@Transactional
public class TruckService {
    private TruckMapper truckMapper;
    private ModelMapper modelMapper;

    @Autowired
    public TruckService(TruckMapper truckMapper, ModelMapper modelMapper) {
        this.truckMapper = truckMapper;
        this.modelMapper = modelMapper;
    }

    public List<TruckDto> getTrucks() {
        List<Truck> fetched = truckMapper.getAll();
        Type targetType = new TypeToken<List<TruckDto>>() {
        }.getType();
        return modelMapper.map(fetched, targetType);
    }

    public Optional<TruckDto> getTruckById(long id) {
        return Optional.ofNullable(truckMapper.getTruckById(id))
                .map(t -> modelMapper.map(t, TruckDto.class));
    }

    public TruckDto createTruck(TruckDto truckDto) {
        Truck truck = modelMapper.map(truckDto, Truck.class);
        try {
            truckMapper.createTruck(truck);
        } catch (DuplicateKeyException e) {
            throw new NameAlreadyExistsException(e);
        }
        long truckId = truck.getId();
        truckMapper.addColors(truckId, truck.getColors());
        truckMapper.addApplicationSegments(truckId, truck.getApplicationSegments());
        return modelMapper.map(truck, TruckDto.class);
    }

    public TruckDto updateTruck(long truckId, TruckDto truckDto) {
        Truck truck = modelMapper.map(truckDto, Truck.class);
        try {
            int rowsUpdated = truckMapper.updateTruck(truckId, truck);
            if (rowsUpdated == 0) {
                throw new NotFoundException("Truck not found");
            }
        } catch (DuplicateKeyException e) {
            throw new NameAlreadyExistsException(e);
        }
        truckMapper.deleteAllColorsBut(truckId, truck.getColors());
        truckMapper.insertNonexistentColors(truckId, truck.getColors());
        truckMapper.deleteAllApplicationSegmentsBut(truckId, truck.getApplicationSegments());
        truckMapper.insertNonexistentApplicationSegments(truckId, truck.getApplicationSegments());
        Truck updatedTruck = truckMapper.getTruckById(truckId);
        return modelMapper.map(updatedTruck, TruckDto.class);
    }

    public void deleteTruck(Long truckId) {
        int rowsUpdated = truckMapper.deleteTruck(truckId);
        if (rowsUpdated == 0) {
            throw new NotFoundException("Truck not found");
        }
    }
}