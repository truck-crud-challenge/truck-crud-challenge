package com.jakomulski.truckcrudchallenge.exceptions;

public class NameAlreadyExistsException extends RuntimeException {
    public NameAlreadyExistsException(Throwable cause) {
        super(cause);
    }
}
