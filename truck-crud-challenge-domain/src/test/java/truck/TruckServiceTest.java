package truck;

import com.jakomulski.truckcrudchallenge.app.db.entities.Truck;
import com.jakomulski.truckcrudchallenge.app.truck.TruckDto;
import com.jakomulski.truckcrudchallenge.app.db.mappers.TruckMapper;
import com.jakomulski.truckcrudchallenge.app.truck.TruckService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TruckServiceTest {

    private TruckService truckService;

    @Mock
    private TruckMapper truckMapper;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        truckService = new TruckService(truckMapper, new ModelMapper());
    }

    @Test
    public void shouldGetEmptyTruckList() {
        when(truckMapper.getAll()).thenReturn(new ArrayList<>());

        List<TruckDto> expected = new ArrayList<>();
        List<TruckDto> actual = truckService.getTrucks();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldGetTruckList() {
        when(truckMapper.getAll()).thenReturn(List.of(
                createTestTruck(1), createTestTruck(2)
        ));

        List<Long> expected = List.of(1l, 2l);
        List<Long> actual = truckService.getTrucks().stream().map(t -> t.getId()).collect(Collectors.toList());
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldNotFindTruckById() {
        when(truckMapper.getTruckById(1)).thenReturn(null);
        Optional<TruckDto> expected = Optional.empty();
        Optional<TruckDto> actual = truckService.getTruckById(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldFindTruckById() {
        when(truckMapper.getTruckById(1)).thenReturn(createTestTruck(1));
        long expected = 1;
        long actual = truckService.getTruckById(1).get().getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    void shouldCreateTruck() {
        Mockito.doAnswer(invocation -> {
            Truck arg = invocation.getArgument(0);
            arg.setId(1);
            return null;
        }).when(truckMapper).createTruck(any(Truck.class));
        TruckDto actual = truckService.createTruck(new TruckDto());
        Assert.assertEquals(1, actual.getId());
    }

    private Truck createTestTruck(long id) {
        Truck truck = new Truck();
        truck.setId(id);
        truck.setName("name");
        return truck;
    }
}